import React from 'react';
import ReactDom from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';
import { Players } from './../imports/api/players';

const renderPlayers = (playersList) => {
  return playersList.map(player => {
    return (
      <p key={player._id}>
        {player.name} has {player.score} point(s).
        <button
          onClick={() => {
            Players.update(player._id, {$inc: {score: 1}});
            }
          }>
          +1
        </button>
        <button
          onClick={() => {
            Players.update(player._id, {$inc: {score: -2}});
            }
          }>
          -1
        </button>
        <button
          onClick={() => {
            Players.remove(player._id);
            }
          }>
          X
        </button>
      </p>
    );
  });
};

const handleSubmit = (e) => {
  const playerName = e.target.playerName.value;

  e.preventDefault();

  if (playerName) {
    e.target.playerName.value = '';
    Players.insert({
      name: playerName,
      score: 0,
    });
  }
};

Meteor.startup(() => {
  Tracker.autorun(() => {
    let players = Players.find().fetch();
    const title = 'Score Keep';
    const name = 'Anddddre';
    const jsx = (
      <div>
        <h1>{title}</h1>
        <p>Hell {name}</p>
        <p>Here is second p</p>
        {renderPlayers(players)}
        <form onSubmit={handleSubmit}>
          <input type="text" name="playerName" placeholder="Player name" />
          <button>Add Player</button>
        </form>
      </div>
    );
    ReactDom.render(jsx, document.getElementById('app'));
  });
});
